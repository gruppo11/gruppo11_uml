package classes;

import java.io.Serializable;




public class Phase implements Serializable{

	private int _idPhase;
	private int _idProject;
	private String _deadline;
	private String _dipendente;
	private boolean _isFinished;
	public int getIdPhase() {
		return _idPhase;
	}
	public void setIdPhase(int idPhase) {
		this._idPhase = idPhase;
	}
	public int getIdProject() {
		return _idProject;
	}
	public void setIdProject(int idProject) {
		this._idProject = idProject;
	}
	public String getDeadline() {
		return _deadline;
	}
	public void setDeadline(String deadline) {
		this._deadline = deadline;
	}
	public String getDipendente() {
		return _dipendente;
	}
	public void setDipendente(String dipendente) {
		this._dipendente = dipendente;
	}
	public boolean isFinished() {
		return _isFinished;
	}
	public void setFinished(boolean isFinished) {
		this._isFinished = isFinished;
	}
	
	public Phase (int idPhase, int idProject, String deadline, String dipendente, boolean isFinished){
		
		_idPhase= idPhase;
		_idProject= idProject;
		_deadline= deadline;
		_dipendente= dipendente;
		_isFinished= isFinished;	
		
	}
}
