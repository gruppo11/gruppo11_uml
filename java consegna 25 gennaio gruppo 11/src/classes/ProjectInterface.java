package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ProjectInterface extends Remote {

	public boolean addProject( Project project) throws RemoteException;
	public  ArrayList<Integer> getIdProjectsList()throws RemoteException;
	public boolean deleteProject(int idProject)throws RemoteException;
	public Project getProject (int idProject)throws RemoteException, SQLException;
	public double getTotCosts ( int idProject)throws RemoteException;
	public boolean setFinished ( int idProject, boolean isFinished)throws RemoteException;
	public boolean setClosed ( int idProject, boolean isclosed)throws RemoteException;
	public boolean updateProject(Project project)throws RemoteException;
	public boolean isFinishedCalculate ( int idProject)throws RemoteException;
	public boolean setMaterialeProgetto( int idProject, int idMateriale, double qta)throws RemoteException;
	
	
	public boolean addOrder ( int idProject, String idCustomer, double sellPrice, String status) throws RemoteException;
	public boolean modifyOrder ( int idProject,String idCustomer, double sellPrice, String status) throws RemoteException;
	public double  getSellPrice( int idProject,String idCustomer)throws RemoteException;
	public Order getOrder ( int idProject,String idCustomer)throws RemoteException;
}


