package classes;

import java.io.Serializable;

public class Personale implements Serializable{
	private int _idPersonale;
	private int _idProject;
	private String _ruolo;
	public int get_idPersonale() {
		return _idPersonale;
	}
	public void set_idPersonale(int _idPersonale) {
		this._idPersonale = _idPersonale;
	}
	public int get_idProject() {
		return _idProject;
	}
	public void set_idProject(int _idProject) {
		this._idProject = _idProject;
	}
	public String get_ruolo() {
		return _ruolo;
	}
	public void set_ruolo(String _ruolo) {
		this._ruolo = _ruolo;
	}

	public void Personale(int idProject, int idPersonale, String ruolo){
		_idProject=idProject;
		_idPersonale=idPersonale;
		_ruolo=ruolo;
	}
	
}
