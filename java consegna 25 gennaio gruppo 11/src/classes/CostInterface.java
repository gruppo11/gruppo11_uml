package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;



public interface CostInterface extends Remote {

	public boolean addVoiceOfCost( int idProject, int idPhase, int idVoiceOfCost, double amount, String type, String description) throws RemoteException;
	public boolean deleteVoiceOfCost ( int idProject, int idPhase, int idVoiceOfCost)throws RemoteException;
	public double getTotalCost( int idProject)throws RemoteException;
	public double getMaterialCost (int idProject)throws RemoteException;
	public double getEmployeeCostOfPhase (int idProject, int idPhase)throws RemoteException;
	public double getOtherCostOfPhase ( int idProject, int idPhase)throws RemoteException;
	public ArrayList<Integer> getIdCost( int idProject, int idPhase)throws RemoteException;
	public VoiceOfcost getCostById ( int idVoiceOfCost, int idPhase, int idProject)throws RemoteException;
}











