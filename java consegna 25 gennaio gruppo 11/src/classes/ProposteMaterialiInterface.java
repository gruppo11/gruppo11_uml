package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ProposteMaterialiInterface extends Remote {

	public boolean addMaterialeProgetto( int idProject, int qta, int idMateriale) throws RemoteException;
	public boolean addFormatoProposta( int idProject, int idProposta, String proposta, String formato) throws RemoteException;
	public boolean addPropostaStand( int idProject, int idProposta, int capienza, int metratura, String link, String luogo) throws RemoteException, SQLException;
	public boolean addServizi(int idProject, int idServizio,  String servizio) throws RemoteException;
	public boolean addPersonale( int idProject, int idPersonale,  String ruolo) throws RemoteException;
	public boolean addProgramma( int idProject, int idProgramma, String prog) throws RemoteException;
	public boolean addMateriale(int idMateriale, double costo, String nome) throws RemoteException;
	public ArrayList<Integer> getListaMatprogetto ( int idProject) throws RemoteException;
	public ArrayList<Integer> getListaProposte( int idProject) throws RemoteException;
	public ArrayList<Integer> getListaProposteStand ( int idProject) throws RemoteException;
	public ArrayList<Integer> getMateriali () throws RemoteException;
	public ArrayList<String> getProgramma( int idProject) throws RemoteException;
	public ArrayList<String> getServizi( int idProject) throws RemoteException;
	public ArrayList<String> getPersonale( int idProject) throws RemoteException;
	public boolean deleteMaterialeProgetto( int idProject, int idMateriale) throws RemoteException;
	public boolean deleteMateriale( int idMateriale) throws RemoteException;
	public boolean deleteProposta( int idProject, int idProposta) throws RemoteException;
	public boolean deletePropostaStand( int idProject, int idProposta) throws RemoteException;
	public boolean deleteProgramma( int idProject, int idProgramma) throws RemoteException;
	public boolean deleteServizio(int idProject, int idServizio) throws RemoteException;
	public boolean deletePersonale( int idProject, int idPersonale) throws RemoteException;
	public int getQtaMatProgetto(int idProject, int idMateriale) throws RemoteException;
	
	public Proposta getproposta( int idProject, int idProposta) throws RemoteException;
	public PropostaStand getpropostaStand( int idProject, int idProposta) throws RemoteException;
}


