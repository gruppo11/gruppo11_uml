package classes;

import java.io.Serializable;


public class StandProject extends Project implements Serializable{
	
	private String _nome;
	private String _location;
	private PropostaStand _propostaStand;
	

	public StandProject(int idProject, String nameProject, String creationDate,
			String custumer, String deadline, String manager,String tipo, String nome, String location) {
		super(idProject, nameProject, creationDate, custumer, deadline, manager, tipo);
		_nome=nome;
		_location=location;
		_propostaStand=null;
		
		// TODO Auto-generated constructor stub
	}
	

	
	


	public String get_nome() {
		return _nome;
	}


	public void set_nome(String _nome) {
		this._nome = _nome;
	}


	public String get_location() {
		return _location;
	}


	public void set_location(String _location) {
		this._location = _location;
	}
	


}
