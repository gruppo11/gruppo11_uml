package classes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.io.Serializable;
import java.sql.*;

public class Project implements Serializable{
	
	protected int _idProject;
	protected String _nameProject;
	protected String _creationDate;
	protected Proposta _proposta;
	protected String _tipo;
	
	protected String _custumer;
	protected String _deadline;
	protected boolean _isFinished;
	protected boolean _isClosed;
	protected double _ricavo;
	protected String _manager;
	
	public Project(int idProject, String nameProject,
			String creationDate, String costumer,
			String deadline, String manager, String tipo) {
		
		_tipo=tipo;
		_idProject = idProject;
		_nameProject = nameProject;
		_creationDate = creationDate;
		_custumer = costumer;
		_deadline = deadline;
		_manager = manager;
		_proposta=null;
	}
	
	public Proposta getProposta() {
		return _proposta;
	}
	public void setProposta(Proposta proposta) {
		_proposta = proposta;
	}
	
	public int getIdProject() {
		return _idProject;
	}
	public void setIdProject(int idProject) {
		_idProject = idProject;
	}
	public String getNameProject() {
		return _nameProject;
	}
	public void setNameProject(String nameProject) {
		_nameProject = nameProject;
	}
	public String getCreationDate() {
		return _creationDate;
	}
	public void setCreationDate(String creationDate) {
		_creationDate = creationDate;
	}
	public String getCostumer() {
		return _custumer;
	}
	public void setCostumer(String costumer) {
		_custumer = costumer;
	}
	public String getDeadline() {
		return _deadline;
	}
	public void setDeadline(String deadline) {
		_deadline = deadline;
	}
	public boolean isFinished() {
		return _isFinished;
	}
	public void setFinished(boolean isFinished) {
		_isFinished = isFinished;
	}
	public boolean isClosed() {
		return _isClosed;
	}
	public void setClosed(boolean isClosed) {
		_isClosed = isClosed;
	}
	public double getRicavo() {
		return _ricavo;
	}
	public void setRicavo(double ricavo) {
		_ricavo = ricavo;
	}
	public String getManager() {
		return _manager;
	}
	public void setManager(String manager) {
		_manager = manager;
	}
	/*
	public Project (int idProject, String customer, String manager){
		
		this.idProject = idProject;
	
		this.custumer = custumer;
	
		this.manager = manager;
		this.proposta=null;
		
	}*/
	


	public String get_nome() {
		return null;
	}



	public void set_nome(String _nome) {
		
	}




	public void set_location(String _location) {
		
	}
	
	


	public String get_location() {
		return null;
	}

	
	public String getTipo() {
		return _tipo;
	}
	public void setTipo(String tipo) {
		_tipo = tipo;
	}
	public void setFinished(int int1) {
		// TODO Auto-generated method stub
		if (int1==1){
			_isFinished=true;
		}
		else
		{
			_isFinished=false;
		}
		
	}
	public void setClosed(int int1) {
		// TODO Auto-generated method stub
		if (int1==1){
			_isClosed=true;
		}
		else
		{
			_isClosed=false;
		}
	}
	
	/*
	public static int[] getProjectsList(String connectionString, String dataUser,String dataPwd){
		
    	Connection connection = null;
		Statement command = null;
		
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	int[]idProgetti = null;
    	ResultSet data;

    		try {
    			
	 		data=command.executeQuery("SELECT idProject FROM projects"); 
	 		int i=0;
	 		if (data.first()){  

	 			idProgetti[i]= data.getInt("idProject");
	 			i=i+1;
	 		}
	 			
	 		
	 			while (data.next()){
	 				idProgetti[i]= data.getInt("idProject");
		 			i=i+1;
		 			
	 			}
	 			
	 			
	 		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return idProgetti;
		
		
		
	}
	
*/
}
