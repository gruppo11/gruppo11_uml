package classes;

import java.io.Serializable;

public class Customer implements Serializable{
	
	private String _idCustomer;
	private String _contact;
	private String _mail;
	private String _cell;
	
	public Customer (String idCustomer, String contact, String mail, String cell){
		_idCustomer=idCustomer;
		_contact=contact;
		_mail=mail;
		_cell=cell;
		
		
	}



	public String get_idCustomer() {
		return _idCustomer;
	}

	public void set_idCustomer(String _idCustomer) {
		this._idCustomer = _idCustomer;
	}

	public String get_contact() {
		return _contact;
	}

	public void set_contact(String _contact) {
		this._contact = _contact;
	}

	public String get_mail() {
		return _mail;
	}

	public void set_mail(String _mail) {
		this._mail = _mail;
	}

	public String get_cell() {
		return _cell;
	}

	public void set_cell(String _cell) {
		this._cell = _cell;
	}

	

}
