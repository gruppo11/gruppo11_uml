package classes;

import java.io.Serializable;

public class Order implements Serializable{
	
	private int _idProject;
	private String _idCustomer;
	private double _sellPrice;
	private String _status;
	
	
	public Order(int idProject, String idCustomer, double sellPrice,String status){
		_idProject=idProject;
		_idCustomer=idCustomer;
		_sellPrice=sellPrice;
		_status=status;
		
	}
	public int get_idProject() {
		return _idProject;
	}
	public void set_idProject(int _idProject) {
		this._idProject = _idProject;
	}
	public String get_idCustomer() {
		return _idCustomer;
	}
	public void set_idCustomer(String _idCustomer) {
		this._idCustomer = _idCustomer;
	}
	public double get_sellPrice() {
		return _sellPrice;
	}
	public void set_sellPrice(double _sellPrice) {
		this._sellPrice = _sellPrice;
	}
	public String get_status() {
		return _status;
	}
	public void set_status(String _status) {
		this._status = _status;
	}


}
