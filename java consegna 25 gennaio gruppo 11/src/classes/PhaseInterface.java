package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface PhaseInterface extends Remote{
	
	public boolean addPhase ( int phaseId, int projectId, String dipendente, String deadline, boolean isFinished) throws RemoteException;
	public boolean deletePhase ( int phaseId, int projectId) throws RemoteException;
	public ArrayList<Integer> getPhaseList (int projectId) throws RemoteException;
	public Phase getPhase(int projectId, int phaseId) throws RemoteException;
	public boolean updatePhase ( int projectId, Phase phase) throws RemoteException;
	
}


