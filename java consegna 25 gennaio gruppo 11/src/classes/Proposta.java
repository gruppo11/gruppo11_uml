package classes;

import java.io.Serializable;

public class Proposta implements Serializable {
	
	private int idproject;
	private int idproposta;
	private String proposta;
	private String formato;
	public int getIdproject() {
		return idproject;
	}
	public void setIdproject(int idproject) {
		this.idproject = idproject;
	}
	public int getIdproposta() {
		return idproposta;
	}
	public void setIdproposta(int idproposta) {
		this.idproposta = idproposta;
	}
	public String getProposta() {
		return proposta;
	}
	public void setProposta(String proposta) {
		this.proposta = proposta;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	public Proposta(int idproject_, int idproposta_, String proposta_, String formato_){
		idproject=idproject_;
		idproposta=idproposta_;
		proposta=proposta_;
		formato=formato_;
		
		
	}

}
