package classes;

import java.io.Serializable;

public class Programma implements Serializable {
	
	private int _idProject;
	private int _idProgramma;
	private String _prog;
	public int get_idProject() {
		return _idProject;
	}
	public void set_idProject(int _idProject) {
		this._idProject = _idProject;
	}
	public int get_idProgramma() {
		return _idProgramma;
	}
	public void set_idProgramma(int _idProgramma) {
		this._idProgramma = _idProgramma;
	}
	public String get_prog() {
		return _prog;
	}
	public void set_prog(String _prog) {
		this._prog = _prog;
	}
	
	public void Programma(int idProject, int idProgramma, String prog){
		_idProject=idProject;
		_idProgramma=idProgramma;
		_prog=prog;
		
	}

}
