package classes;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CustomerInterface extends Remote, Serializable{
	
	 public boolean addCustomer(Customer customer) throws RemoteException;
	 public boolean deleteCustomer(String idcustomer) throws RemoteException;
	 public String getCustomers()throws RemoteException;
	 public ArrayList<String> getCustomersStringList()throws RemoteException;
	 public String prova()throws RemoteException;
}

