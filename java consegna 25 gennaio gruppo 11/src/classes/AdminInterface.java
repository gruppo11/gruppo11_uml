package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.ArrayList;



public interface AdminInterface extends Remote {


    public boolean writeOnUsersSql (String user, String pwd, String accessYype)throws RemoteException;
    public boolean deleteOnUserSql ( String user)throws RemoteException;
    public ArrayList<String> getDipendentiManager( char dipMan)throws RemoteException;    
    public boolean setExtPrivileges(String iduser, int maxAcc) throws RemoteException;
    public boolean setExtAccess(String iduser, int acc) throws RemoteException;
    public int getExtAccess(String iduser) throws RemoteException;
    public int getMaxExtAccess(String iduser) throws RemoteException;
    public ArrayList<String> getExtUser()throws RemoteException;    
    public boolean deleteAllTablesInDatabase()throws RemoteException;    

}


