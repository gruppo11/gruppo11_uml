package server;
import java.io.*;
import java.util.*;




public class FileManager {
	
	
	public static void newFile(String nome, String estensione) {
		 /*Se il file nno esiste ne crea uno nuovo */
		 try {
		 File file = new File((nome.concat(".")).concat(estensione)); 
		 

		 if (file.exists()){
			 
			 return;}
		 
		 
		 else if (file.createNewFile());
		 
		 else
		 System.out.println("Il file "+ file + "non pu� essere creato");
		 }
		 catch (IOException e) {
		 e.printStackTrace();
		 }
		}
	
	 public static void writeFile(String nomeFile, String estensione, String contenuto) {
		 //scrivo tutto il contenuto di una stringa su un file con una certa estensione, nel caso sovrascrivo il precedente contenuto (???)
		 String path = nomeFile+"."+estensione;
		 try {
		 File file = new File(path);
		 
		 if (!file.exists()){
			 
			 return;}
		 
		 FileWriter fw = new FileWriter(file);
		 BufferedWriter bw = new BufferedWriter(fw);
		 bw.write(contenuto);
		 bw.flush();
		 bw.close();
		 }
		 catch(IOException e) {
		 e.printStackTrace();
		 }
		 }
	 
	 public static ArrayList<String> createRowArrayList(String line) {
     //Separo una linea in diverse stringhe da mettere in un array
		 ArrayList<String> arr= new ArrayList<String> ();
		 
		 
		 
		 String cell="";
		 int numCell=0;
		 
		
		 for (int i=0; i<line.length();i=i+1){
			
			 
			 if (line.charAt(i)!=';'){
				/* System.out.println("valore di i in if "+i + "arr vale "+arr.size());*/
				cell=cell+line.charAt(i); 			 
			 }
			 else{
				 /*System.out.println("valore di cell "+cell );*/
				 
				 /*
				 output[numCell]=cell;
				 */
				 
				 arr.add(cell);
				 /*System.out.println("valore di i in esle "+i + "arr vale "+arr.size());*/
				 
		
				 cell="";
				 
				 
				 numCell=numCell+1;
				 
		
			 }
			
			 
			 
		 }
		 if (cell!=""){
		 arr.add(cell); 
	 }
	
	
	return arr;
		 
		 
		 }
	 
	 public static ArrayList<ArrayList<String>> readFile(String nome) throws IOException {
		 /*funzione di livello alto*/
		 //restituisce il contenuto di un file csv sotto forma di matrice
		 /*prende in ingresso il nome di un file csv compresa estensione, ritorna una lista di array di lista di array/*
		 /* indice 1= riga, indice 2= colonna*/

		 ArrayList<ArrayList<String>> tableArray2D = new ArrayList<ArrayList<String>> ();
		 
		 BufferedReader br = new BufferedReader(new FileReader(nome));
		 try {
		     StringBuilder sb = new StringBuilder();
		     String line = "";/* br.readLine();*/
		     /*System.out.println("valore di line: " + line);*/
		     int row=0;
		     
		     ArrayList<String> rowArray;/* = new ArrayList<String> ();*/
		    
	    	
		     
		     while (line != null) {
		    
		    	 
		    	 
		    	 line = br.readLine();
		    	 /*System.out.println("valore di line in while: " + line);*/
		    	 
		    	 
		    	 
		    	 if (line!=null){
		    		/* System.out.println("entra in if ");*/
		    	 
		    		 
		    		/* System.out.println("valore di row: " + row +" valore di line: " +line);*/
		    		 row=row+1;
		         
		    		 /* NB => separatore di colonna = ;*/
		         
		         
		    		 /*creo arrayList di stringhe (riga) */
		    		 
		    		 rowArray=createRowArrayList(line);
		    		/* System.out.println("rowArray: " + rowArray);*/
		    		 
		         
		    		 /*inserisco arrayList di stringhe*/
		    		 tableArray2D.add(rowArray);
		    		/* System.out.println("tableArray2D.size "+tableArray2D.size());*/
		    	 };
		         
		         
		         
		     }
		     /*String everything = sb.toString();*/
		     
		 } finally {
		     br.close();
		     
		 }
		 return tableArray2D;
		 
		 }
	 
	 public static void printTable2D(ArrayList<ArrayList<String>> tableArray2D) {
		 /*funzione di diagnostica*/

		 
		 for (int i=0; /* numero righe */ i<tableArray2D.size(); i=i+1){
			 for (int j=0; j<tableArray2D.get(i).size(); j=j+1){
				 
				 System.out.println("riga "+i+" colonna "+j+" contenuto = "+tableArray2D.get(i).get(j));
			 }
				 
		 }
		 
		 
		 }
	 
	 public static void writeFileFromArrayList(String nameFile,String estensione, ArrayList<ArrayList<String>> tableArray2D) {
		 
		 
		 /*Scrivo una lista (dove � salvato un progetto o dati cliente) in un file csv. NB nameFile va passato senza estensione*/
		 
		 /* CREO FILE
		  * 
		  * TRASFORMO CONTENUTO IN UNA STRINGA DOVE SEPARATORE DI COLONNA E' ; E SEPARATORE DI LINEA E' \n
		  * 
		  * USO LA WRITE FILE
		  */
		
		 newFile(nameFile,estensione);
		 String content = ""; 
		 for (int i=0; i<tableArray2D.size(); i=i+1){
			 
			 for (int j=0; j<tableArray2D.get(i).size(); j=j+1){
				 content=content+tableArray2D.get(i).get(j)+";";
				 
			 }
			 content=content+"\n";
		 }
		/* System.out.println("content "+content);*/
		 
		 /*writeFile(nameFile,content);*/
		 
		 /*System.out.println("nameFile "+nameFile);*/
		 
		 writeFile(nameFile,estensione, content  );
		 
		 
		 }
	 
	 public static String fromStringArrayToString(String[] lista){
		
		 String out="";
		 if (lista!=null){
			 
			 for (int i=0; i<lista.length; i=i+1){
				 
				 out=out+lista[i]+";";
			 }
			 return out;
		 }
		 
		 
		 return null;
		 
		
		 
		 
	 }

	

}
