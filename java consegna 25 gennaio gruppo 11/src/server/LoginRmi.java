package server;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import classes.LoginInterface;

public class LoginRmi extends UnicastRemoteObject implements LoginInterface, Serializable{
    public LoginRmi() throws RemoteException 
    {
        
    }
    
    public String getConnectString (String host, String dataName)throws RemoteException{
    	
    	return ("jdbc:mysql://"+host+"/"+dataName);
    }
    

    
    
    public String setLoginFromData( String user, char[] pwd, int pwdAcc) throws RemoteException{
    	System.out.println("entro in setloginfromdata..");	
    	Connection connection = null;
		Statement command = null;
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	ResultSet data;
    	String pwdTemp="";
    	
    	// eseguire qua la conversione in sha256
   /*
    	pwdTemp.valueOf(pwd);
    	String pwdsalata = pwdTemp+"xj34876hs";
    	MessageDigest digest = null;// = new MessageDigest();
		try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	byte[] hash = digest.digest(pwdsalata.getBytes(StandardCharsets.UTF_8));
		pwdTemp=hash.toString(); */
		// fine conversione in sha256
		
		//*****************
		pwdTemp=pwdTemp.valueOf(pwd);
		//******************
		
    		try {
    			
	 		data=command.executeQuery("SELECT * FROM users"); 
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			System.out.println("data.getString(pwd) "+data.getString("pwd"));
	 			if (user.contentEquals(data.getString("idusers"))){
	 				if (pwdTemp.contentEquals(data.getString("pwd"))){
	 					if (pwdAcc==0){
	 						return (pwdTemp);
	 					}
	 					if (pwdAcc==1){
	 						return (data.getString("accessType"));
	 					}
	 					
	 					}
	 				}
	 			}
	 			
	 			while (data.next()){
	 				if (user.contentEquals(data.getString("idusers"))){
		 				if (pwdTemp.contentEquals(data.getString("pwd"))){
		 					if (pwdAcc==0){
		 						return (pwdTemp);
		 					}
		 					if (pwdAcc==1){
		 						return (data.getString("accessType"));
		 					}
		 					
		 					}
		 				}
		 			
	 			}
	 		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return null;
    	
    	
    }

	


	
@SuppressWarnings("null")
public String setLoginPwdAccess(String user, char[] pwd, int pwdAcc)throws RemoteException {
	//System.out.println("entro setlogin");
	ArrayList<String> log=new ArrayList<String>();
	log=null;
	String output=null;
		
		if(user==null||pwd==null){

			return null;
		}

		String pwdTemp="";
		pwdTemp=pwdTemp.valueOf(pwd);
		
		FileManager.newFile("users", "csv");
		ArrayList<ArrayList<String>> tableArray2D = new ArrayList<ArrayList<String>>();
		 try {
			tableArray2D=FileManager.readFile("users.csv");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (tableArray2D.isEmpty()){
			return null;
		}
		
		
		
		log=findUser(user, tableArray2D);
		
		
		
		
		if (log==null){
		
			return null;
			
		}
		if (log!=null&& !log.isEmpty()){
		if (log.get(0).contains(pwdTemp)){
			
			//output.set(0, "admin");
			//output.set(1, "a");
			output=log.get(pwdAcc);
			return output;
		}
		}
		return output;
		
	}



public static ArrayList<String> findUser (String user,ArrayList<ArrayList<String>> tableArray2D){
	
	ArrayList<String> out=new ArrayList<String>();
	
	
	/*System.out.println(tableArray2D.get(0).get(0));
	System.out.println("prova3");*/
	if (tableArray2D.get(0).isEmpty()){
		
		
			
			return (null);
		}
	
	for (int i=0; i<tableArray2D.size(); i=i+1){
		
		
		
		
		
		if(tableArray2D.get(i).get(0).contentEquals(user)){
			
			out.add(0, tableArray2D.get(i).get(1));
			out.add(1, tableArray2D.get(i).get(2));
			
			
			return out;
		}
	}
	return null;
	
}

public String prova() throws RemoteException {
	// TODO Auto-generated method stub
	return "prova fatta";
}

public byte[] setLoginFromdataHash( String user, char[] pwd) throws RemoteException {
	Connection connection = null;
	Statement command = null;
	
	String connectionString= DbUtilities.getConnectionString();
	String dataUser=DbUtilities.getUser();
	String dataPwd=DbUtilities.getPwd();
	try {
		connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	
	try {
		command=connection.createStatement();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	
	ResultSet data;
	String pwdTemp="";
	
	// eseguire qua la conversione in sha256

	pwdTemp.valueOf(pwd);
	String pwdsalata = pwdTemp+"xj34876hs";
	MessageDigest digest = null;// = new MessageDigest();
	try {
		digest = MessageDigest.getInstance("SHA-256");
	} catch (NoSuchAlgorithmException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	byte[] hash = digest.digest(pwdsalata.getBytes(StandardCharsets.UTF_8));
	
	// fine conversione in sha256
	

	
		try {
			
 		data=command.executeQuery("SELECT * FROM users"); 
 		
 		if (data.first()){  //controllo che user non esisti di gia
 			System.out.println("data.getString(pwd) "+data.getString("pwd"));
 			if (user.contentEquals(data.getString("idusers"))){
 				if (pwdTemp.contentEquals(data.getString("pwd"))){
 					
 						return (hash);
 				
 					
 					}
 				}
 			}
 			
 			while (data.next()){
 				if (user.contentEquals(data.getString("idusers"))){
	 				if (pwdTemp.contentEquals(data.getString("pwd"))){
	 					
	 						return (hash);
	 					
	 					
	 					
	 					}
	 				}
	 			
 			}
 		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return null;
}


    

}
