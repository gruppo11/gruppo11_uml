package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import classes.Phase;
import classes.PhaseInterface;

public class PhaseRmi extends UnicastRemoteObject implements PhaseInterface, Serializable{

	protected PhaseRmi() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean addPhase( int phaseId, int projectId, String dipendente,
			String deadline, boolean isFinished) throws RemoteException {
		
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(phaseId) +",'"+deadline+"',"+ Integer.toString(projectId)  + ",'"+ dipendente+"',"+Boolean.toString(isFinished) +")");
    	try {
    		
    		command.execute("INSERT INTO phases (idphase,deadline,idproject,idusers,isFinished) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deletePhase(int phaseId, int projectId)
			throws RemoteException {
		// TODO Auto-generated method stub
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			command.executeUpdate("DELETE FROM phases WHERE idphase = "+Integer.toString(phaseId));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public ArrayList<Integer> getPhaseList( int projectId) throws RemoteException {
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<Integer> out=new ArrayList<Integer>();
    	
    		try {
    			
	 		data=command.executeQuery("SELECT idphase  from phases WHERE idproject=  " + String.valueOf(projectId)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getInt("idphase"));
	 			System.out.println("data.first phase..."+(data.getInt("idphase")));
	 			
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getInt("idphase"));
	 			
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
		
		
	}
	

	public Phase getPhase(int projectId, int phaseId)
			throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	//Phase phase = null;

    		try {
    			
	 		data=command.executeQuery("SELECT *  from phases WHERE idproject =  " + String.valueOf(projectId)+ " and idphase= "+String.valueOf(phaseId)); 

	 		
	 		data.first() ; 
	 		Phase phase=new Phase(data.getInt("idphase"), data.getInt("idproject"), data.getString("deadline"), data.getString("idusers"), data.getBoolean("isFinished"));
	 		return phase;
	 			
}
	 			
		
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			return null;	
	}

	public boolean updatePhase( int projectId, Phase phase)
			throws RemoteException {
		//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			

				command.executeQuery("UPDATE phases SET deadline ='" +phase.getDeadline() +"' SET isFinished= "+phase.isFinished()+  "WHERE  idphase="+Integer.toString(phase.getIdPhase())+ "and idproject = "+Integer.toString(phase.getIdProject()) );	
				
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}



}
