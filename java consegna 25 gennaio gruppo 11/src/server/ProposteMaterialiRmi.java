package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import classes.Personale;
import classes.Programma;
import classes.Proposta;
import classes.PropostaStand;
import classes.ProposteMaterialiInterface;
import classes.Servizi;

public class ProposteMaterialiRmi extends UnicastRemoteObject implements ProposteMaterialiInterface, Serializable{
    public ProposteMaterialiRmi() throws RemoteException 
    {
        
    }

	public boolean addMaterialeProgetto( int idProject, int qta,
			int idMateriale) throws RemoteException {
		
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(idProject)+","+Integer.toString(idMateriale)+","+ Integer.toString(qta) + ")");
    	try {
    		
    		command.execute("INSERT INTO materialiProgetto (idproject,idmateriale,quantita) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
		
	}

	public boolean addFormatoProposta(int idProject, int idProposta,
			String proposta, String formato) throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(idProposta)+","+Integer.toString(idProject)+",'"+ formato +"','"+ proposta + "')");
    	try {
    		
    		command.execute("INSERT INTO proposta (idproposta,idproject,formato,proposta) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean addPropostaStand( int idProject, int idProposta, int capienza,
			int metratura, String link, String luogo) throws RemoteException, SQLException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    	ResultSet data;
    	ArrayList<Integer> out=new ArrayList<Integer>();
    	
    	
    			
	 		
				data=command.executeQuery("SELECT idpropostaStand  from propostaStand WHERE idproject =  " + Integer.toString(idProject));
		
	 		if (data.first()){
	 			
	 			if (data.getInt("idpropostaStand")==idProposta){
	 				return false;
	 			}
	 		}
	 		
	 		while (data.next()){
	 			
	 			if (data.getInt("idpropostaStand")==idProposta){
	 				return false;
	 			}
	 		}
    	
    
    	String values= ("("+Integer.toString(idProposta)+","+Integer.toString(idProject)+",'"+ link +"',"+ metratura+","+ capienza+",'"+ luogo + "')");
    	try {
    		
    		command.execute("INSERT INTO propostaStand (idpropostaStand,idproject,link,metratura,capienza,luogo) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean addServizi(int idProject, int idServizio, String servizio)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(idServizio)+","+Integer.toString(idProject)+",'"+ servizio + "')");
    	try {
    		
    		command.execute("INSERT INTO servizi (idservizio,idproject,servizio) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean addPersonale( int idProject, int idPersonale, String ruolo)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(idPersonale)+","+Integer.toString(idProject)+",'"+ ruolo + "')");
    	try {
    		
    		command.execute("INSERT INTO personale (idpersonale,idproject,ruolo) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean addProgramma( int idProject, int idProgramma, String prog)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(idProgramma)+","+Integer.toString(idProject)+",'"+ prog + "')");
    	try {
    		
    		command.execute("INSERT INTO programma (idprogramma,idproject,prog) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean addMateriale( int idMateriale, double costo, String nome)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	

    	
    
    	String values= ("("+Integer.toString(idMateriale)+","+Double.toString(costo)+",'"+ nome + "')");
    	try {
    		
    		command.execute("INSERT INTO materiali (idmateriale,costo,nome) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public ArrayList<Integer> getListaMatprogetto( int idProject) throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<Integer> out=new ArrayList<Integer>();
  
    		try {
    			
	 		data=command.executeQuery("SELECT idmateriale  from materialiProgetto WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getInt("idmateriale"));
	 		
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getInt("idmateriale"));
	 		
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<Integer> getListaProposte( int idProject) throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<Integer> out=new ArrayList<Integer>();
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT idproposta  from proposta WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getInt("idproposta"));
	 			
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getInt("idproposta"));
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<Integer> getListaProposteStand( int idProject)
			throws RemoteException {
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<Integer> out=new ArrayList<Integer>();
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT idpropostaStand  from propostaStand WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getInt("idpropostaStand"));
	 			
	 			}
	 		
	 		while (data.next()){
				out.add(data.getInt("idpropostaStand"));
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<Integer> getMateriali() throws RemoteException {
		
	Connection connection=null;
	String connectionString= DbUtilities.getConnectionString();
	String dataUser=DbUtilities.getUser();
	String dataPassword=DbUtilities.getPwd();
	try {
		connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	Statement command = null;
	try {
		command=connection.createStatement();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	ResultSet data;
	ArrayList<Integer> out=new ArrayList<Integer>();
	int i=0;
		try {
			
 		data=command.executeQuery("SELECT idmateriale  from materiali "); 
 		
 		
 		if (data.first()){  //controllo che user non esisti di gia
 			out.add(data.getInt("idmateriale"));
 			
 			}
 		
 		while (data.next()){
 			out.add(data.getInt("idmateriale"));
			}
 			

 		
	} 
catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return out;	
	}

	public ArrayList<String> getProgramma( int idProject) throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out=new ArrayList<String>();
    	//int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT prog  from programma WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getString("prog"));
	 		//	i=i+1;
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getString("prog"));
	 		//	i=i+1;
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<String> getServizi( int idProject) throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out=new ArrayList<String>();
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT servizio  from servizi WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getString("servizio"));
	 			i=i+1;
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getString("servizio"));
	 			i=i+1;
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<String> getPersonale( int idProject) throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out=new ArrayList<String>();
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT ruolo  from personale WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getString("ruolo"));
	 			i=i+1;
	 			}
	 		
	 		while (data.next()){
	 			out.add(data.getString("ruolo"));
	 			i=i+1;
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public Proposta getproposta( int idProject, int idProposta)
			throws RemoteException {
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	Proposta proposta=null;
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT * from proposta WHERE idproject =  " + String.valueOf(idProject)+  " and idproposta = "+String.valueOf(idProposta) ); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			//(int idproject_, int idproposta_, String proposta_, String formato_){
	 			proposta=new Proposta(data.getInt("idproject"),data.getInt("idproposta"),data.getString("proposta"),data.getString("formato"));
	 			;
	 			}
	 		
	 	
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return proposta;	
	}

	public PropostaStand getpropostaStand( int idProject, int idProposta)
			throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	/*ResultSet data;
    	ResultSet data1;
    	ResultSet data2;*/
    	ResultSet data3;
    	
    	/*
    	ArrayList<Servizi> out =new ArrayList<Servizi>();
    	ArrayList<Programma> programma =new ArrayList<Programma>();
    	ArrayList<Personale> personale =new ArrayList<Personale>();
    	
    	//PropostaStand propostastand=null;
    	//int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT *  from servizi WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data.first()){  
	 			Servizi ser = new Servizi();
	 			ser.set_servizio(data.getString("servizio")) ;
	 			ser.set_idServizio(data.getInt("idservizio"));
	 			ser.set_idProject(data.getInt("idproject"));
	 			out.add(ser);
	 			}
	 		
	 		while (data.next()){
	 			Servizi ser = new Servizi();
	 			ser.set_servizio(data.getString("servizio")) ;
	 			ser.set_idServizio(data.getInt("idservizio"));
	 			ser.set_idProject(data.getInt("idproject"));
	 			out.add(ser);
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		
        	//i=0;
    		try {
    			
	 		data1=command.executeQuery("SELECT *  from programma WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data1.first()){  //controllo che user non esisti di gia
	 			Programma prog=new Programma();
	 			prog.set_prog(data1.getString("prog")) ;
	 			prog.set_idProgramma(data1.getInt("idprogramma"));
	 			prog.set_idProject(data1.getInt("idproject"));
	 			programma.add(prog);
	 			//i=i+1;
	 			}
	 		
	 		while (data1.next()){
	 			Programma prog=new Programma();
	 			prog.set_prog(data1.getString("programma")) ;
	 			prog.set_idProgramma(data1.getInt("idprog"));
	 			prog.set_idProject(data1.getInt("idproject"));
	 			programma.add(prog);
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    		
    		
        	
    		try {
    			
	 		data2=command.executeQuery("SELECT *  from personale WHERE idproject =  " + String.valueOf(idProject)); 
	 		
	 		
	 		if (data2.first()){  //controllo che user non esisti di gia
	 			Personale pers= new Personale();
	 			pers.set_ruolo((data2.getString("ruolo"))) ;
	 			pers.set_idPersonale(data2.getInt("idpersonale"));
	 			pers.set_idProject(data2.getInt("idproject"));
	 			personale.add(pers);
	 			//i=i+1;
	 			}
	 		
	 		while (data2.next()){
	 			Personale pers= new Personale();
	 			pers.set_ruolo((data2.getString("ruolo"))) ;
	 			pers.set_idPersonale(data2.getInt("idpersonale"));
	 			pers.set_idProject(data2.getInt("idproject"));
	 			personale.add(pers);
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    		/*
    		idProject=idProject_;
    		idPropostaStand=idPropostaStand_;
    		link=link_;
    		metratura=metratura_;
    		capienza=capienza_;
    		programma=programma_;
    		personale=personale_;
    		servizi=servizi_;	*/
    		
    		try {
    			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
    		} catch (SQLException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
        	
        	// i=0;
        		try {
        			
    	 		data3=command.executeQuery("SELECT *  from propostaStand WHERE idproject =  " + String.valueOf(idProject)+ " and idpropostaStand = "+ String.valueOf(idProposta)); 
    	 		
    	 		/*(int idPropostaStand_, int idProject_, String link_, String metratura_, String capienza_, Programma[] programma_, Personale[] personale_,
			Servizi[] servizi_)*/
    	 		
    	 		
    	 		if (data3.first()){  //controllo che user non esisti di gia
    	 			PropostaStand propostastand=new PropostaStand(idProposta, idProject,data3.getString("link"), data3.getString("metratura"), data3.getString("capienza"));
    	 			return propostastand;	
     			}
    	 			

    	 		
    		} 
    	catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}	
    		
    		
    		return null;
			
	}

	public boolean deleteMaterialeProgetto( int idProject, int idMateriale)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM materialiProgetto WHERE idproject = "+ String.valueOf(idProject)+ " and idmateriale = "+ String.valueOf(idMateriale));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deleteMateriale( int idMateriale) throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM materiali WHERE idmateriale = "+ String.valueOf(idMateriale));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deleteProposta( int idProject, int idProposta)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM proposta WHERE idproject = "+ String.valueOf(idProject)+ " and idproposta = "+ String.valueOf(idProposta));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deletePropostaStand( int idProject, int idProposta)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM propostaStand WHERE idproject = "+ String.valueOf(idProject)+ " and idpropostaStand = "+ String.valueOf(idProposta));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deleteProgramma( int idProject, int idProgramma)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM programma WHERE idproject = "+ String.valueOf(idProject)+ "and idprogramma = "+ String.valueOf(idProgramma));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deleteServizio( int idProject, int idServizio)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM servizi WHERE idproject = "+ String.valueOf(idProject)+ " and idservizio = "+ String.valueOf(idServizio));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deletePersonale( int idProject, int idPersonale)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	/*DELETE FROM table_name
    	WHERE some_column = some_value*/
    	
    
    	
    	try {
    		
    		command.execute("DELETE FROM personale WHERE idproject = "+ String.valueOf(idProject)+ " and idpersonale = "+ String.valueOf(idPersonale));
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public int getQtaMatProgetto( int idProject, int idMateriale)
			throws RemoteException {
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data = null;
    	int out=0;
    	int i=0;
    		
    			
	 		try {
				data=command.executeQuery("SELECT quantita  from materialiProgetto WHERE idproject =  " + String.valueOf(idProject)+" and idmateriale = "+String.valueOf(idMateriale));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	 		
	 		
	 		try {
				if (data.first()){  //controllo che user non esisti di gia
					out=data.getInt("quantita");
					
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return out;
 
}


}