package server;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import classes.Order;
import classes.Project;
import classes.ProjectInterface;
import classes.StandProject;

public class ProjectRmi extends UnicastRemoteObject implements ProjectInterface, Serializable{
    public ProjectRmi() throws RemoteException 
    {
        
    }

	public ArrayList<Integer> getIdProjectsList() throws RemoteException {
		
    	Connection connection = null;
		Statement command = null;
		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	ArrayList<Integer> idProgetti = new ArrayList<Integer>();
    	ResultSet data;

    		try {
    			
	 		data=command.executeQuery("SELECT idProject FROM projects"); 
	 		int i=0;
	 		if (data.first()){  

	 			idProgetti.add(data.getInt("idProject"));
	 			i=i+1;
	 		}
	 			
	 		
	 			while (data.next()){
	 				idProgetti.add(data.getInt("idProject"));
		 			i=i+1;
		 			
	 			}
	 			
	 			
	 		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return idProgetti;
		
		
		
	}
    

	public boolean addProject(Project project) throws RemoteException {
		// TODO Auto-generated method stub
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	
    	ResultSet data;
		try {
			
 		data=command.executeQuery("SELECT idproject FROM projects"); 
 		
 		if (data.first()){  //controllo che project non esisti di gia
 			if (project.getIdProject()== (data.getInt("idProject"))){
 				return false;
 			}
 			
 			while (data.next()){
 				if (project.getIdProject()== (data.getInt("idProject"))){
	 				return false;
	 			}
	 			
 			}
 		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		System.out.println("customer id in addProject= "+project.getCostumer());
		String values= ("('"+project.getIdProject()+"','"+project.getNameProject()+"','"+project.getCreationDate()+"','"+project.getCostumer()+"','"+project.getDeadline()+"','"+project.getManager()+"','"+project.getTipo()+"')");
		
    	
     	
    	try {
    		command.execute("INSERT INTO projects (idProject,nameProject,creationDate, idcustomers,deadline,iduser,tipo) VALUES"+values);
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    		
		
		String values1= "";
		if (project.getTipo().contentEquals("stand")){
			try {
				values1=("("+project.getIdProject()+",'"  +project.get_nome()+"','"+project.get_location()+"')");
				command.execute("INSERT INTO standProject (idProject,nome,location) VALUES"+values1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
    	

		
		
		return true;
	}



	public boolean deleteProject( int idProject) throws RemoteException {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement command = null;
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			command.executeUpdate("DELETE FROM projects WHERE idProject = "+String.valueOf(idProject) );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public Project getProject( int idProject) throws RemoteException, SQLException {
		// TODO Auto-generated method stub
	
		System.out.println("sono in get project");
    	Connection connection=null;
    	String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	
    	//ResultSet data = null;
		
			
 		
 			ResultSet data=command.executeQuery("SELECT * FROM projects where idProject = "+String.valueOf(idProject) );
 			//data.first();
	
 			System.out.println("ho eseguito query in get project");
 		
 		/*	(int idProject, String nameProject, String creationDate,
 					String custumer, String deadline, String manager,String tipo, String nome, String location)*/
 		
 		if (data.first()&&(!data.getString("tipo").contentEquals("stand"))){
 			Project out = new Project(idProject, data.getString("nameProject"),data.getString("creationDate") , data.getString("idcustomers"),data.getString("deadline"),data.getString("iduser"), data.getString("tipo"));
 			out.setFinished(data.getInt("isFinished"));
 			out.setClosed(data.getInt("isClosed"));
 			if (data.getInt("isClosed")==1){
 		 		try {
 					out.setRicavo(data.getInt("ricavo"));
 				} catch (SQLException e) {
 					// TODO Auto-generated catch block
 					e.printStackTrace();
 				}
 		 		}
 			return out;
 		}
 		else
 		{
 			if (data.first()){
 			StandProject out= new StandProject(idProject, data.getString("nameProject"), data.getString("creationDate"), data.getString("idcustomers"), data.getString("deadline"), data.getString("iduser"), data.getString("tipo"), "nome", "location");
 			
 			out.setFinished(data.getInt("isFinished"));
 			out.setClosed(data.getInt("isClosed"));
 			if (data.getInt("isClosed")==1){
 		 		try {
 					out.setRicavo(data.getInt("ricavo"));
 				} catch (SQLException e) {
 					// TODO Auto-generated catch block
 					e.printStackTrace();
 				}
 		 		
 		 		}
 			ResultSet data1=command.executeQuery("SELECT * FROM standProject where idProject = "+String.valueOf(idProject) );
 			if (data1.first()){
 			out.set_nome(data1.getString("nome"));
 			out.set_location(data1.getString("location"));
 			}
 			return out;
 			}
 			
 		}
 			

		return null;
 		
	
		
	}

	public double getTotCosts( int idProject) throws RemoteException {

    	Connection connection = null;
		Statement command = null;
		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	double out=0;
    	ResultSet data;

    		try {
    			
	 		data=command.executeQuery("SELECT sum(amount) as total FROM phases join voiceOfCost on phases.idphase = voiceOfCost.idphase WHERE voiceOfCost.idproject=" + String.valueOf(idProject)); 
	 		
	 		if (data.first()){  

	 			out=out+ data.getDouble("total");
	 			
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    		
        	ResultSet data1;

    		try {
    			
	 		data1=command.executeQuery("SELECT sum(quantita*costo) as total FROM materialiProgetto join materiali on materialiProgetto.idmateriale = materiali.idmateriale WHERE idproject=" + String.valueOf(idProject)); 
	 		
	 		if (data1.first()){  

	 			out=out+ data1.getDouble("total");
	 			
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			return out;		
    		
    		
    		
	}

	public boolean setFinished( int idProject, boolean isFinished) throws RemoteException {
		
		//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
			Connection connection=null;
			try {
				connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		//-----------------------------------------------
			
			
			Statement command = null;
			
			
			try {
				command=connection.createStatement();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				
				command.executeQuery("UPDATE projects SET isFinished =" +isFinished+  "WHERE  idproject="+Integer.toString(idProject) );
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return true;

	}

	public boolean setClosed( int idProject, boolean isClosed) throws RemoteException {
		//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			
			command.executeQuery("UPDATE projects SET isClosed =" +isClosed+  "WHERE  idproject="+Integer.toString(idProject) );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public boolean updateProject( Project project) throws RemoteException {
		//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String type=project.getTipo();
		try {
			
			if (!type.contentEquals("stand")){
			command.executeUpdate("UPDATE projects SET isClosed = " + String.valueOf(project.isClosed())  +", nameProject = '"+project.getNameProject() +"',  creationDate = '"+project.getCreationDate()+"', idcustomers = '"+project.getCostumer() +"', deadline = '"+project.getDeadline() +"',iduser = '"+ project.getManager() +"', isFinished = "+String.valueOf(project.isFinished())  + " WHERE  idproject = "+Integer.toString(project.getIdProject()) );
			}
			else {
				command.executeUpdate("UPDATE projects SET isClosed = " +String.valueOf(project.isClosed()) +", nameProject = ' "+project.getNameProject()+"', creationDate = '"+project.getCreationDate()+"',idcustomers = '"+project.getCostumer() +"', deadline = '"+project.getDeadline() +"',iduser ='"+ project.getManager() +"', isFinished = "+String.valueOf(project.isFinished()) +  " WHERE  idproject = "+Integer.toString(project.getIdProject()) );	
				command.executeUpdate("UPDATE standProjects SET  location = '"+project.get_location() +"', nome = '"+project.get_nome() + "' WHERE  idproject = "+Integer.toString(project.getIdProject()) );
			}
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public boolean isFinishedCalculate( int idProject)
			throws RemoteException {
    	Connection connection = null;
		Statement command = null;
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	ArrayList<String> out=new ArrayList<String>();
    	ResultSet data;
    	int i=0;

    		try {
    			
	 		data=command.executeQuery("SELECT isFinished  FROM phases  WHERE idproject=" + String.valueOf(idProject)); 
	 		
	 		if (data.first()){  

	 			out.add(String.valueOf(data.getBoolean("isFinished"))) ;
	 			
	 		}
	 		while (data.next()){  

	 			out.add(String.valueOf(data.getBoolean("isFinished"))) ;
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    		System.out.println("out = "+out);
    		for (int j=0; j<out.size(); j=j+1){
    			if (out.get(i).contentEquals("0")||out.get(i).contentEquals("false")){
    				return false;
    			}
    		}
return true;
			
	}

	public boolean setMaterialeProgetto( int idProject, int idMateriale, double qta)
			throws RemoteException {
    	Connection connection = null;
		Statement command = null;
		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPwd=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	ResultSet data;
    	

    		try {
    			
	 		data=command.executeQuery("SELECT quantita  FROM materialiProgetto  WHERE idproject = " + Integer.toString(idProject)+ " and idmateriale = "+Integer.toString(idMateriale)); 
	 		
	 		
	 		
	 		if (!data.first()){  //se la query � vuota 
	 			//INSERT INTO tbl_name (col1,col2) VALUES(col2*2,15);
	 			command.execute("INSERT INTO materialiProgetto (idproject, idmateriale, quantita) VALUES (" + String.valueOf(idProject)+","+ String.valueOf(idMateriale)+","+String.valueOf(qta)+" )"); 
	 		}
	 		
	 		else {
	 			
	 			command.executeUpdate("UPDATE materialiProgetto SET quantita = "+String.valueOf(qta)+" WHERE idproject = "+ String.valueOf(idProject)+" and idmateriale = "+ String.valueOf(idMateriale)); 
		 		
	 			
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

return true;
	}
	
	
	//**************************************
	
	public boolean addOrder( int idProject, String idCustomer,
			double sellPrice, String status) throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("('"+ status + "',"+ sellPrice+")");
    	try {
    		
    		command.execute("INSERT INTO projects (projects.status,projects.sellPrice) VALUES "+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean modifyOrder( int idProject, String idCustomer,
			double sellPrice, String status) throws RemoteException {
		
	// TODO Auto-generated method stub
	
//------GENERO CONNESSIONE AL DATABASE --------		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
	Connection connection=null;
	try {
		connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
//-----------------------------------------------
	
	
	Statement command = null;
	
	
	try {
		command=connection.createStatement();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	try {

		
		command.executeQuery("UPDATE projects SET sellPrice = " +String.valueOf(sellPrice)+ " SET orderstatus = " + status+ " WHERE idcustomer = "+ idCustomer  + "and idproject="+Integer.toString(idProject) );
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	return true;
	
}

	public double getSellPrice( int idProject, String idCustomer)
			throws RemoteException {
    	Connection connection = null;
		Statement command = null;
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	ResultSet data;

    		try {
    			
    			
	 		data=command.executeQuery("SELECT sellPrice FROM projects  WHERE idproject = " + String.valueOf(idProject)+ " and idcustomers = '"+idCustomer+"'"); 
    			
	 		if (data.first()){  

	 			return (data.getDouble("sellPrice"));
	 			
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		
		
		
		// TODO Auto-generated method stub
		return 0;
	}

	public Order getOrder( int idProject, String idCustomer)
			throws RemoteException {
    	Connection connection = null;
		Statement command = null;
		
		String connectionString= DbUtilities.getConnectionString();
		String dataUser=DbUtilities.getUser();
		String dataPassword=DbUtilities.getPwd();
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	//System.out.println("entro in setloginfromdata..");	
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	ResultSet data;
    	//Order order=null;

    		try {

	 		data=command.executeQuery("SELECT * FROM customers join projects on customers.idcustomer = projects.idcustomers WHERE projects.idproject = '"+ idProject+ "'"); 
	 		
	 		if (data.first()){  
//int idProject, String idCustomer, double sellPrice,String status)
	 			
	 			Order order=new Order(data.getInt("idproject"), data.getString("idcustomer"), data.getDouble("sellPrice"), data.getString("orderstatus"));
	 			/*order.set_sellPrice(data.getDouble("sellPrice"));
	 			order.set_status(data.getString("status"));
	 			order.set_idCustomer(data.getString("idcustomer"));
	 			order.set_idProject(data.getInt("idproject"));*/
	 			return order;
	 			
	 		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		
		
		
		// TODO Auto-generated method stub
		return null;
	}






    
}