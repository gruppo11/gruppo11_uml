package testing;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Test;

import classes.AdminInterface;
import classes.CostInterface;
import classes.Customer;
import classes.CustomerInterface;
import classes.LoginInterface;
import classes.PhaseInterface;
import classes.Project;
import classes.ProjectInterface;
import classes.ProposteMaterialiInterface;
import classes.StandProject;
import client.FileManagerClient;
import client.MainWindow;
import server.DbUtilities;
import server.LoginRmi;



public class CostTest {
	
	
	
	private String _address="rmi://localhost:1099";
	private String _dataUser="";
	private String _dataPwd="";
	private String _connectionString ="";

	@SuppressWarnings("deprecation")
	@Test
	public void test() {
		
		_connectionString= DbUtilities.getConnectionString();
		_dataUser=DbUtilities.getUser();
		_dataPwd=DbUtilities.getPwd();
//*************************
		
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(_connectionString,_dataUser,_dataPwd);
		} catch (SQLException e2) {
		
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
		
			e1.printStackTrace();
		}

    	AdminInterface adminInt=null;
		try {
			adminInt = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e3) {
		
			e3.printStackTrace();
		} catch (RemoteException e3) {
		
			e3.printStackTrace();
		} catch (NotBoundException e3) {
		
			e3.printStackTrace();
		}
		
		try {
			adminInt.deleteAllTablesInDatabase();
		} catch (RemoteException e4) {
		
			e4.printStackTrace();
		}
		
		
		try {
			adminInt.writeOnUsersSql( "admin", "admin", "a");
		} catch (RemoteException e3) {
		
			e3.printStackTrace();
		}
		try {
			adminInt.writeOnUsersSql( "managerprova", "managerprova", "m");
		} catch (RemoteException e3) {
			
			e3.printStackTrace();
		}
		try {
			adminInt.writeOnUsersSql( "dipendenteprova", "dipendenteprova", "d");
		} catch (RemoteException e3) {
	
			e3.printStackTrace();
		}
		
		CustomerInterface custInt = null;

/*
	*/	
		
		try {
			custInt = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
		} catch (MalformedURLException e3) {
		
			e3.printStackTrace();
		} catch (RemoteException e3) {
		
			e3.printStackTrace();
		} catch (NotBoundException e3) {
		
			e3.printStackTrace();
		}

		ArrayList<String> list = new ArrayList<String>();
		Customer customer=new Customer( "prova", "a", "a", "a");
		
		 
			 try {
				custInt.addCustomer( customer);
			} catch (RemoteException e3) {
			
				e3.printStackTrace();
			}
			 

				ProjectInterface proInt=null;
				
				try {
					proInt = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e2) {
				
					e2.printStackTrace();
				} catch (RemoteException e2) {
				
					e2.printStackTrace();
				} catch (NotBoundException e2) {
			
					e2.printStackTrace();
				}
	
				Project project=new Project(-1, "a", "a", "prova", "a", "managerprova", "on line");
				
				project.setCostumer("prova");
				project.setIdProject(-1);
				project.setManager("managerprova");

 

				try {
					proInt.addProject( project);
				} catch (RemoteException e1) {
				
					e1.printStackTrace();
				}


 
				CostInterface costInt = null;

				
				try {
					costInt = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
		
					e1.printStackTrace();
				} catch (RemoteException e1) {
			
					e1.printStackTrace();
				} catch (NotBoundException e1) {
				
					e1.printStackTrace();
				}

				PhaseInterface phaseInt=null;
				
				try {
					phaseInt = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
				} catch (MalformedURLException e1) {
				
					e1.printStackTrace();
				} catch (RemoteException e1) {
		
					e1.printStackTrace();
				} catch (NotBoundException e1) {
				
					e1.printStackTrace();
				}
				
				try {
					phaseInt.addPhase( -1, -1, "dipendenteprova", "aprile", false);
				} catch (RemoteException e) {
		
					e.printStackTrace();
				}
				
				try {
					costInt.addVoiceOfCost( -1, -1, -1, 10, "employee", "employee cost");
				} catch (RemoteException e) {
	
					e.printStackTrace();
				}
				
				try {
					costInt.addVoiceOfCost( -1, -1, -2, 15,  "employee", "employee cost");
				} catch (RemoteException e) {

					e.printStackTrace();
				}
				
				ProposteMaterialiInterface propMatInt=null;
				
				try {
					propMatInt=(ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e) {
		
					e.printStackTrace();
				} catch (RemoteException e) {
			
					e.printStackTrace();
				} catch (NotBoundException e) {
	
					e.printStackTrace();
				}
				
				try {
					propMatInt.addMateriale( -1, 5, "materiale prova");
				} catch (RemoteException e) {
				
					e.printStackTrace();
				}
				
				try {
					propMatInt.addMaterialeProgetto( -1, 2, -1);
				} catch (RemoteException e) {
				
					e.printStackTrace();
				}
				
				double output=0;
				try {
					 output= costInt.getTotalCost( -1);
				} catch (RemoteException e) {
				
					e.printStackTrace();
				}
				

				assertEquals(35, output,0.01);
			 
	}

}