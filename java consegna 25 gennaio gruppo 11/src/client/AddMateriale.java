package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javafx.scene.control.ComboBox;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

public class AddMateriale extends JFrame {

	private JPanel contentPane;
	private JTextField idtxt;
	private JTextField nometxt;
	private JTextField costotxt;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JButton btnExit;
	private JLabel label;
	private JComboBox comboBox;


	public AddMateriale(String connectionString, String address, String dataUser, String dataPwd) {
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 812, 219);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Id");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(24, 22, 61, 24);
		contentPane.add(lblNewLabel);
		
		idtxt = new JTextField();
		idtxt.setBounds(71, 24, 111, 24);
		contentPane.add(idtxt);
		idtxt.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNome.setBounds(232, 22, 61, 24);
		contentPane.add(lblNome);
		
		nometxt = new JTextField();
		nometxt.setColumns(10);
		nometxt.setBounds(312, 24, 111, 24);
		contentPane.add(nometxt);
		
		JLabel lblCosto = new JLabel("Costo");
		lblCosto.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCosto.setBounds(474, 20, 61, 24);
		contentPane.add(lblCosto);
		
		costotxt = new JTextField();
		costotxt.setColumns(10);
		costotxt.setBounds(554, 22, 111, 24);
		contentPane.add(costotxt);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.addMateriale(Integer.valueOf(idtxt.getText()) ,Double.valueOf(costotxt.getText()) ,nometxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}

			
			
		});
		btnNewButton.setBounds(71, 81, 89, 23);
		contentPane.add(btnNewButton);
		
		btnExit = new JButton("Exit");

		btnExit.setBounds(184, 81, 89, 23);
		contentPane.add(btnExit);
		
		label = new JLabel("Id");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(362, 81, 61, 24);
		contentPane.add(label);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(420, 80, 133, 24);
		contentPane.add(comboBox);
		
		JButton btnDelete = new JButton("Delete");
		
		btnDelete.setBounds(576, 81, 89, 23);
		contentPane.add(btnDelete);
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				closewin();
			}
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.deleteMateriale( Integer.valueOf((String)comboBox.getSelectedItem()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();	
				
			}
		});
		
		
		
		updatecombobox();
		
	}
	private void closewin() {
		// TODO Auto-generated method stub
		updatecombobox();
		this.setVisible(false);
	}
	
	
	private void updatecombobox(){
		
		comboBox.removeAll();
		ArrayList<Integer> lista = new ArrayList<Integer>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getMateriali();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i));
		}
	}
	
}
