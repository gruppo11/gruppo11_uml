package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JButton;

public class PhaseModify extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PhaseModify frame = new PhaseModify();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PhaseModify() {
		setTitle("Modify/Create Phase");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 610);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Project Number");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(29, 37, 130, 24);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setBounds(180, 37, 143, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPhaseNumber = new JLabel("Phase Number");
		lblPhaseNumber.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPhaseNumber.setBounds(356, 37, 130, 24);
		contentPane.add(lblPhaseNumber);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_1.setColumns(10);
		textField_1.setBounds(492, 37, 143, 24);
		contentPane.add(textField_1);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 100, 660, 2);
		contentPane.add(separator);
		
		JLabel lblDeadline = new JLabel("Deadline");
		lblDeadline.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDeadline.setBounds(29, 143, 95, 24);
		contentPane.add(lblDeadline);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_2.setColumns(10);
		textField_2.setBounds(135, 143, 188, 24);
		contentPane.add(textField_2);
		
		JLabel lblEmployee = new JLabel("Employee");
		lblEmployee.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEmployee.setBounds(29, 193, 95, 24);
		contentPane.add(lblEmployee);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_3.setColumns(10);
		textField_3.setBounds(135, 193, 188, 24);
		contentPane.add(textField_3);
		
		JButton btnNewButton = new JButton("Submit Phase To Manager");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(29, 260, 262, 24);
		contentPane.add(btnNewButton);
		
		JButton btnAssignPhaseTo = new JButton("Assign Phase to Employee");
		btnAssignPhaseTo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAssignPhaseTo.setEnabled(false);
		btnAssignPhaseTo.setBounds(320, 260, 262, 24);
		contentPane.add(btnAssignPhaseTo);
		
		JButton btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSave.setEnabled(false);
		btnSave.setBounds(29, 338, 89, 24);
		contentPane.add(btnSave);
	}

}
