package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddStand extends JFrame {

	private JPanel contentPane;
	private JTextField idpropostatxt;
	private JTextField capienzatxt;
	private JTextField metraturatxt;
	private JTextField linktxt;
	private JTextField luogotxt;

	private int _idProject;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JLabel label;
	private JComboBox comboBox;
	private JButton btnDelete;
	private JButton btnExit;
	
	public AddStand(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 811, 291);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdProposta = new JLabel("Id proposta");
		lblIdProposta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdProposta.setBounds(10, 99, 100, 24);
		contentPane.add(lblIdProposta);
		
		idpropostatxt = new JTextField();
		idpropostatxt.setColumns(10);
		idpropostatxt.setBounds(116, 99, 100, 24);
		contentPane.add(idpropostatxt);
		
		capienzatxt = new JTextField();
		capienzatxt.setColumns(10);
		capienzatxt.setBounds(353, 99, 100, 24);
		contentPane.add(capienzatxt);
		
		JLabel lblCapienza = new JLabel("Capienza");
		lblCapienza.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCapienza.setBounds(247, 99, 100, 24);
		contentPane.add(lblCapienza);
		
		JLabel lblMetratura = new JLabel("Metratura");
		lblMetratura.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMetratura.setBounds(490, 99, 100, 24);
		contentPane.add(lblMetratura);
		
		metraturatxt = new JTextField();
		metraturatxt.setColumns(10);
		metraturatxt.setBounds(596, 99, 100, 24);
		contentPane.add(metraturatxt);
		
		JLabel lblLink = new JLabel("Link");
		lblLink.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLink.setBounds(247, 156, 100, 24);
		contentPane.add(lblLink);
		
		linktxt = new JTextField();
		linktxt.setColumns(10);
		linktxt.setBounds(353, 156, 100, 24);
		contentPane.add(linktxt);
		
		JLabel lblLuogo = new JLabel("Luogo");
		lblLuogo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLuogo.setBounds(490, 156, 100, 24);
		contentPane.add(lblLuogo);
		
		luogotxt = new JTextField();
		luogotxt.setColumns(10);
		luogotxt.setBounds(596, 156, 100, 24);
		contentPane.add(luogotxt);
		
		JButton btnNewButton = new JButton("Add");

		btnNewButton.setBounds(490, 202, 89, 23);
		contentPane.add(btnNewButton);
		
		label = new JLabel("Id proposta");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(10, 25, 100, 24);
		contentPane.add(label);
		
		comboBox = new JComboBox();
		comboBox.setBounds(130, 25, 120, 24);
		contentPane.add(comboBox);
		
		btnDelete = new JButton("Delete");
	
		btnDelete.setBounds(277, 26, 100, 23);
		contentPane.add(btnDelete);
		
		btnExit = new JButton("Exit");
	
		btnExit.setBounds(607, 202, 89, 23);
		contentPane.add(btnExit);
		
		updatecombobox();
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					try {
						e.addPropostaStand( _idProject,Integer.valueOf(idpropostatxt.getText()) ,Integer.valueOf(capienzatxt.getText())  , Integer.valueOf(metraturatxt.getText()) , linktxt.getText(), luogotxt.getText());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				updatecombobox();
				
			}
		});
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
		
				closewin();
				
			}
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.deletePropostaStand( _idProject,Integer.valueOf((String)comboBox.getSelectedItem()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
				
			}
		});
		
		
	}

	private void updatecombobox(){
		
		comboBox.removeAll();
		ArrayList<Integer> lista = new ArrayList<Integer>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getListaProposteStand(_idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i));
		}
	}
	
	protected void closewin() {
		// TODO Auto-generated method stub
		updatecombobox();
		this.setVisible(false);
	}
}
