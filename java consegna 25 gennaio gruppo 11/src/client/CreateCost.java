package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

import classes.CostInterface;
import classes.PhaseInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class CreateCost extends JFrame {

	private JPanel contentPane;
	private JTextField costtxt;
	private JTextField amounttxt;
	private JTextField desctxt;
	private int _idProject;
	private int _idPhase;
	private String _address;
	private String _connectionString;
	private String _dataUser;
	private String _dataPwd;
	private JComboBox comboBox;


	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateCost frame = new CreateCost();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public CreateCost(int idProject, int idPhase, String address, String connectionString, String dataUser, String dataPwd) {
		_idPhase=idPhase;
		_idProject=idProject;
		_address=address;
		_connectionString=connectionString;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setTitle("Create Voice Of Cost");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 778, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Code Number");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(31, 30, 126, 24);
		contentPane.add(lblNewLabel);
		
		costtxt = new JTextField();
		costtxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		costtxt.setBounds(182, 32, 157, 24);
		contentPane.add(costtxt);
		costtxt.setColumns(10);
		
		JLabel lblTypeOfCost = new JLabel("Type Of Cost");
		lblTypeOfCost.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTypeOfCost.setBounds(31, 81, 126, 24);
		contentPane.add(lblTypeOfCost);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"employee", "other"}));
		comboBox.setBounds(182, 83, 157, 24);
		contentPane.add(comboBox);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAmount.setBounds(31, 137, 126, 24);
		contentPane.add(lblAmount);
		
		amounttxt = new JTextField();
		amounttxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		amounttxt.setColumns(10);
		amounttxt.setBounds(182, 137, 157, 24);
		contentPane.add(amounttxt);
		
		JLabel lblDescriptiopn = new JLabel("Description");
		lblDescriptiopn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDescriptiopn.setBounds(31, 197, 126, 24);
		contentPane.add(lblDescriptiopn);
		
		desctxt = new JTextField();
		desctxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		desctxt.setColumns(10);
		desctxt.setBounds(182, 197, 369, 24);
		contentPane.add(desctxt);
		
		JButton btnNewButton = new JButton("Save");

		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(31, 278, 89, 24);
		contentPane.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				exitwin();
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(152, 278, 89, 24);
		contentPane.add(btnExit);
		
		
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CostInterface costint = null;
				try {
					costint = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				;
				
				if (costtxt.getText().contentEquals("")||amounttxt.getText().contentEquals("")){
					return;
				}
				
				try {
					costint.addVoiceOfCost( _idProject, _idPhase, Integer.valueOf(costtxt.getText()), Double.valueOf(amounttxt.getText()), (String)comboBox.getSelectedItem(), desctxt.getText());// deletePhase(_connectionString, _dataUser, _dataPwd, (Integer)phasecombo.getSelectedItem()	, (Integer)projectcombo.getSelectedItem());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		
	}


	protected void exitwin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
}
