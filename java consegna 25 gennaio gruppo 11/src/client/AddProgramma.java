package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddProgramma extends JFrame {

	private JPanel contentPane;
	private JTextField idprogtxt;
	private JTextField progtxt;

	private int _idProject;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JLabel label;
	private JComboBox comboBox;
	private JButton btnDelete;
	private JButton btnExit;
	
	public AddProgramma(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdProgramma = new JLabel("Id programma");
		lblIdProgramma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdProgramma.setBounds(25, 37, 124, 24);
		contentPane.add(lblIdProgramma);
		
		idprogtxt = new JTextField();
		idprogtxt.setColumns(10);
		idprogtxt.setBounds(144, 37, 200, 24);
		contentPane.add(idprogtxt);
		
		JLabel lblProgramma = new JLabel("Programma");
		lblProgramma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProgramma.setBounds(25, 95, 124, 24);
		contentPane.add(lblProgramma);
		
		progtxt = new JTextField();
		progtxt.setColumns(10);
		progtxt.setBounds(144, 95, 200, 24);
		contentPane.add(progtxt);
		
		JButton btnNewButton = new JButton("Add");

		btnNewButton.setBounds(25, 147, 89, 23);
		contentPane.add(btnNewButton);
		
		label = new JLabel("Id programma");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(25, 191, 124, 24);
		contentPane.add(label);
		
		comboBox = new JComboBox();
		comboBox.setBounds(176, 191, 168, 24);
		contentPane.add(comboBox);
		
		btnDelete = new JButton("Delete");

		btnDelete.setBounds(369, 194, 114, 23);
		contentPane.add(btnDelete);
		
		btnExit = new JButton("Exit");
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closewin();
			}
		});
		btnExit.setBounds(369, 256, 114, 23);
		contentPane.add(btnExit);
		
		updatecombobox();
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//progtxt idprogtxt
				try {
					e.addProgramma( _idProject,Integer.valueOf(idprogtxt.getText()) , progtxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.deleteProgramma( _idProject,Integer.valueOf(idprogtxt.getText()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
	}
	protected void closewin() {
		// TODO Auto-generated method stub
		updatecombobox();
		this.setVisible(false);
	}
	private void updatecombobox(){
		
		comboBox.removeAllItems();
		ArrayList<String> lista = new ArrayList<String>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getProgramma( _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i) );
		}
	}
}
