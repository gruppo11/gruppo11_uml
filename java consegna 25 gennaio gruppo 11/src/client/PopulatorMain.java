package client;

public class PopulatorMain {
	
	private static String _address="rmi://localhost:1099";
	private static String _dataUser="root";
	private static String _dataPwd="root";
	private String _dataHost="localhost";
	private String _dataName="mydb3";
	private static String _connectionString ="jdbc:mysql://localhost/mydb3";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DatabasePopulator.deleteDatabaseContent(_address, _connectionString, _dataUser, _dataPwd);
		
		DatabasePopulator.populate(_address, _connectionString, _dataUser, _dataPwd);
		
		
	}

}
