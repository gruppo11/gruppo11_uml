package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import classes.AdminInterface;
import classes.CostInterface;
import classes.Customer;
import classes.CustomerInterface;
import classes.LoginInterface;
import classes.Phase;
import classes.PhaseInterface;
import classes.Project;
import classes.ProjectInterface;
import classes.ProposteMaterialiInterface;
import classes.StandProject;



public class DatabasePopulator {
	
	public static void deleteDatabaseContent(String _address){
		
		AdminInterface adminInt = null;
		
		
		try {
			adminInt = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//***********************************************************************************
	
		try {
			adminInt.deleteAllTablesInDatabase();
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	
	public static void populate(String _address){
		
		deleteDatabaseContent(_address);
		ProjectInterface projInt = null;
		
		
		try {
			projInt = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		CostInterface costInt = null;
		
		
		try {
			costInt = (CostInterface)Naming.lookup(_address+"/CostInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		
		ProposteMaterialiInterface propMatInt = null;
		
		
		try {
			propMatInt = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PhaseInterface phaseInt = null;
		
		
		try {
			phaseInt = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		CustomerInterface custInt = null;
		
		
		try {
			custInt = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LoginInterface logInt = null;
		
		
		try {
			logInt = (LoginInterface)Naming.lookup(_address+"/LoginInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		AdminInterface adminInt = null;
		
		try {
			adminInt = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		// ADD users

		try {
			adminInt.writeOnUsersSql( "admin", "admin", "a");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		
		try {
			adminInt.writeOnUsersSql( "manager1", "manager1", "m");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			adminInt.writeOnUsersSql( "managerprova", "managerprova", "m");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			adminInt.writeOnUsersSql( "dipendente1", "dipendente1", "d");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			adminInt.writeOnUsersSql( "admin1", "admin1", "a");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			adminInt.writeOnUsersSql( "manager2", "manager2", "m");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			adminInt.writeOnUsersSql( "dipendente2", "dipendente2", "d");
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		// ADD CUSTOMERS
		Customer customer= new Customer("", "", "", "");
		customer.set_cell("3334545555");
		customer.set_contact("Max Erre");
		customer.set_idCustomer("milanimal");
		customer.set_mail("mil@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		customer.set_cell("3334115555");
		customer.set_contact("Albe Baggio");
		customer.set_idCustomer("magnetimarelli");
		customer.set_mail("magneti@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	/*
	 * 	
	 */
		customer.set_cell("3334545345");
		customer.set_contact("Ludovica Marelli");
		customer.set_idCustomer("porsche");
		customer.set_mail("comai@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		customer.set_cell("3333345555");
		customer.set_contact("Rebecca Ferrari");
		customer.set_idCustomer("djpromoting");
		customer.set_mail("musicaa@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		customer.set_cell("3334548525");
		customer.set_contact("Jhonny Esberg");
		customer.set_idCustomer("novaali");
		customer.set_mail("cucciii@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		customer.set_cell("3334545995");
		customer.set_contact("");
		customer.set_idCustomer("trenord");
		customer.set_mail("milTreNord@hotmail.it");
		
		try {
			custInt.addCustomer( customer);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Customer customer1=new Customer( "Fabriano", "Ludovica Rinaldi", "ludovica@fabriano.it", "3336589858");
		
		 
		 try {
			custInt.addCustomer( customer1);
		} catch (RemoteException e3) {
		
			e3.printStackTrace();
		}
		 
		//aggiungo magteriali
		
		try {
			propMatInt.addMateriale( 1, 10, "carta");
			propMatInt.addMateriale( 2, 15, "inchiostro");
			propMatInt.addMateriale( 3, 20, "scatoloni");
			propMatInt.addMateriale( 4, 30, "colore");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		/*
		 * 
		 */
		// AGGUNGO PROGETTI (int idProject, String nameProject,
		/*String creationDate, String costumer,
		String deadline, String manager, String tipo)*/
		
		Project project1=new Project(1, "Milano challenge", "10 ottobre 2016", "milanimal", "10 luglio 2017", "managerprova", "on line");
		System.out.println("customer... prima di set... "+project1.getCostumer());
		project1.setCostumer("milanimal");
		project1.setIdProject(1);
		project1.setManager("managerprova");



		try {
			projInt.addProject(project1);
		} catch (RemoteException e1) {
		
			e1.printStackTrace();
		}

		

		
Project project11=new Project(-1, "Wodafuck", "10 agosto 2016", "milanimal", "2 marzo 2017", "managerprova", "on line");//, "nome", "location");
		
/*		project11.setCostumer("milanimal");
		project11.setIdProject(-1);
		project11.setManager("managerprova");
		project11.setTipo("on line");
	*/	
		
		
		try {
			projInt.addProject( project11);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*
		project.setClosed(false);
		project.setCostumer("milanimal");
		project.setDeadline("18 aprile");
		project.setManager("manager1");
		project.setNameProject("Milano challange");
		project.setFinished(false);
		project.setTipo("on line");
		project.setCreationDate("1 agosto");
		project.setIdProject(2);
		
		
		try {
			projInt.addProject(_connectionString, dataUser, dataPwd, project);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		
		//int idProject, String nameProject, String creationDate,
		//String custumer, String deadline, String manager,String tipo, String nome, String location)
		StandProject stand= new StandProject(-2, "promozione malpensa", "10 gennaio 2017", "trenord", "luglio 2017", "managerprova", "stand", "nome stand", "location da definire");
		stand.setTipo("stand");
		/*stand.set_location("via valtellina");
		stand.setTipo("stand");
		stand.set_nome("Stramilano");
		stand.setCostumer("porsche");
		stand.setIdProject(3);
		stand.setManager("manager1");
		stand.setDeadline("19 agosto");
		*/
		
		try {
			projInt.addProject( stand);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		stand.set_location("via Muratori");
		stand.set_nome("Wodafuck");
		stand.setCostumer("milanimal");
		stand.setIdProject(4);
		stand.setManager("manager2");
		stand.setTipo("stand");
		stand.setDeadline("19 settembre");
		
		try {
			projInt.addProject(_connectionString, dataUser, dataPwd, stand);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
	// ARRIVATO QUA
	
		
		try {
			phaseInt.addPhase( 1, 1, "dipendente1", "30 maggio", false);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			phaseInt.addPhase( 2, 1, "dipendente1", "30 maggio", false);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
		
		//AGGIUNGO VOCI DI COSTO
		
		try {
			costInt.addVoiceOfCost( 1, 1, 1, 100, "employee", "stipendio");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			costInt.addVoiceOfCost( 1, 2, 2, 100, "employee", "stipendio");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//AGGIUNGO MATERIALI PROGETTO
		//project qta idmateriale
		try {
			propMatInt.addMaterialeProgetto( -1, 10, 1);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			propMatInt.addMaterialeProgetto( -2, 2, 2);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//AGGIUNGO PROPOSTE
		
		try {
			propMatInt.addFormatoProposta( 1, 1, "proposta 1", "300 x 50");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		//AGGIUNGO PERSONALE PROGRAMMA E SERVIZI
		
		try {
			propMatInt.addPersonale( -2, 1, "cameriere");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			propMatInt.addProgramma( -2, 1, "h11 conferenza stampa");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			propMatInt.addServizi(-2, 1, "Buffet");
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//AGGIUNGO PROPOSTE STAND
		
		
		try {
			try {
				propMatInt.addPropostaStand( -2, 1, 50, 15, "www.mywebspace.com", "Alcatraz");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//
		
		
		
		
	}
	
	

}
