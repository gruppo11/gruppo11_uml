package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import classes.ProjectInterface;
import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class AddMaterialeProgetto extends JFrame {

	private JPanel contentPane;
	private int _idProject;
	String _address;
	String _connectionString;
	String _dataUser;
	String _dataPwd;
	private JTextField qtatxt;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddMaterialeProgetto frame = new AddMaterialeProgetto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public AddMaterialeProgetto(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 641, 157);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("Materiale");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblId.setBounds(35, 13, 91, 24);
		contentPane.add(lblId);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		comboBox.removeAllItems();
		comboBox.addItem(3232);
		
		comboBox.setBounds(158, 13, 140, 24);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("Set");
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(333, 13, 140, 24);
		contentPane.add(btnNewButton);
		
		JButton btnAddMaterialeTo = new JButton("Add Materiale to DataBase");
		
		btnAddMaterialeTo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAddMaterialeTo.setBounds(333, 83, 282, 24);
		contentPane.add(btnAddMaterialeTo);
		
		qtatxt = new JTextField();
		qtatxt.setColumns(10);
		qtatxt.setBounds(158, 48, 140, 24);
		contentPane.add(qtatxt);
		
		JLabel lblQta = new JLabel("Qta");
		lblQta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblQta.setBounds(72, 47, 91, 24);
		contentPane.add(lblQta);
		
		JButton btnExit = new JButton("Exit");
		
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(333, 43, 140, 24);
		contentPane.add(btnExit);
		
		JButton btnDelete = new JButton("Delete");
	
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDelete.setBounds(475, 13, 140, 24);
		contentPane.add(btnDelete);
		
		updateElencoMateriali();
		
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//
				ProjectInterface e = null;
				
				
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.setMaterialeProgetto( _idProject, (Integer)comboBox.getSelectedItem(),Integer.valueOf(qtatxt.getText()) );
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				closewin();
			}

			
			
		});
		
		comboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//qtatxt	
				ProposteMaterialiInterface e = null;
				double qta=0;
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					qta= e.getQtaMatProgetto( _idProject, (Integer)comboBox.getSelectedItem() );
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				qtatxt.setText(String.valueOf(qta));
			}
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//cancella materiale progetto
			ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.deleteMaterialeProgetto( _idProject, (Integer)comboBox.getSelectedItem());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updateElencoMateriali();
			}
		});
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closewin();
			}
		});
		btnAddMaterialeTo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//(String connectionString, String address, String dataUser, String dataPwd)
				 AddMateriale win= new AddMateriale(_connectionString,  _address,  _dataUser,  _dataPwd);
				win.setVisible(true);
			}
		});
		
	}

	private void updateElencoMateriali() {
		// TODO Auto-generated method stub
		comboBox.removeAllItems();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<Integer> materiali=new ArrayList<Integer>();
		try {
			materiali=e.getMateriali();
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		for (int i=0; i<materiali.size(); i=i+1){
			comboBox.addItem(materiali.get(i));
		}
	}
	private void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
}
