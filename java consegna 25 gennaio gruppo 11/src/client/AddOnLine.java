package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

public class AddOnLine extends JFrame {

	private JPanel contentPane;
	private JTextField idtxt;
	private JTextField propotxt;
	private JTextField dimtxt;
	private int _idProject;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JButton btnExit;
	private JLabel lblList;
	private JComboBox comboBox;
	private JButton btnDelete;
	
	public AddOnLine(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 302);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		idtxt = new JTextField();
		idtxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		idtxt.setColumns(10);
		idtxt.setBounds(110, 66, 200, 24);
		contentPane.add(idtxt);
		
		JLabel lblId = new JLabel("Id");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblId.setBounds(10, 66, 115, 24);
		contentPane.add(lblId);
		
		propotxt = new JTextField();
		propotxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		propotxt.setColumns(10);
		propotxt.setBounds(110, 124, 200, 24);
		contentPane.add(propotxt);
		
		JLabel lblProposta = new JLabel("Proposta");
		lblProposta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProposta.setBounds(10, 124, 115, 24);
		contentPane.add(lblProposta);
		
		dimtxt = new JTextField();
		dimtxt.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dimtxt.setColumns(10);
		dimtxt.setBounds(110, 194, 200, 24);
		contentPane.add(dimtxt);
		
		JLabel lblDimensione = new JLabel("Dimensione");
		lblDimensione.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDimensione.setBounds(10, 194, 115, 24);
		contentPane.add(lblDimensione);
		
		JButton btnNewButton = new JButton("Save");
	
		btnNewButton.setBounds(390, 229, 89, 23);
		contentPane.add(btnNewButton);
		
		btnExit = new JButton("Exit");
	
		btnExit.setBounds(281, 229, 89, 23);
		contentPane.add(btnExit);
		
		lblList = new JLabel("List");
		lblList.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblList.setBounds(10, 13, 115, 24);
		contentPane.add(lblList);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		comboBox.removeAllItems();
		comboBox.addItem(3232);
		comboBox.setBounds(110, 15, 200, 24);
		contentPane.add(comboBox);
		
		btnDelete = new JButton("Delete");
	
		btnDelete.setBounds(367, 14, 89, 23);
		contentPane.add(btnDelete);
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.addFormatoProposta(_idProject, Integer.valueOf(idtxt.getText()) ,propotxt.getText(), dimtxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				try {
					e.deleteProposta( _idProject, (Integer)comboBox.getSelectedItem());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closewin();
			}

			
		});
		updatecombobox();
	}
	private void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
		
	}
	private void updatecombobox(){
		
		comboBox.removeAllItems();
		ArrayList<Integer> lista = new ArrayList<Integer>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getListaProposte( _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i));
		}
	}
}
