package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddServizio extends JFrame {

	private JPanel contentPane;
	private JTextField idserviziotxt;
	private JTextField serviziotxt;

	private int _idProject;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JLabel label;
	private JComboBox comboBox;
	private JButton btnDelete;
	private JButton btnExit;
	
	public AddServizio(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 647, 333);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdServizio = new JLabel("Id servizio");
		lblIdServizio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdServizio.setBounds(23, 49, 124, 24);
		contentPane.add(lblIdServizio);
		
		idserviziotxt = new JTextField();
		idserviziotxt.setColumns(10);
		idserviziotxt.setBounds(142, 49, 200, 24);
		contentPane.add(idserviziotxt);
		
		JLabel lblServizio = new JLabel("Servizio");
		lblServizio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblServizio.setBounds(23, 101, 124, 24);
		contentPane.add(lblServizio);
		
		serviziotxt = new JTextField();
		serviziotxt.setColumns(10);
		serviziotxt.setBounds(142, 101, 200, 24);
		contentPane.add(serviziotxt);
		
		JButton btnNewButton = new JButton("Add");

		btnNewButton.setBounds(24, 150, 89, 23);
		contentPane.add(btnNewButton);
		
		label = new JLabel("Id servizio");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(23, 200, 124, 24);
		contentPane.add(label);
		
		comboBox = new JComboBox();
		comboBox.setBounds(142, 202, 200, 24);
		contentPane.add(comboBox);
		
		btnDelete = new JButton("Delete");
	
		btnDelete.setBounds(373, 201, 135, 24);
		contentPane.add(btnDelete);
		
		btnExit = new JButton("Exit");
	
		btnExit.setBounds(23, 261, 89, 23);
		contentPane.add(btnExit);
		
		updatecombobox();
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.addServizi(_idProject, Integer.valueOf(idserviziotxt.getText()) , serviziotxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
			
			
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
		ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				try {
					e.deleteServizio(_idProject,Integer.valueOf((String)comboBox.getSelectedItem()) );
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				updatecombobox();
				closewin();
			}
		});
		
		
	}
	protected void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
	private void updatecombobox(){
		
		comboBox.removeAllItems();
		ArrayList<String> lista = new ArrayList<String>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getServizi( _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i) );
		}
	}
}
