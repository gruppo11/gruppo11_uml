package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import classes.AdminInterface;
import classes.LoginInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddUserWindow extends JFrame {

	private JPanel contentPane;
	private JTextField userTxt;
	private JTextField pwdTxt;
	private JTextField textField_2;
	private String _address;
	private String _connectionString;
	private String _dataUser;
	private String _dataPassword;
	
	//private Connection _connection;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddUserWindow frame = new AddUserWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public AddUserWindow(String address, final String connectString,final String dataUser, final String dataPassword, final String dataHost, final String dataName) {
		
		//_connection=connection;
		_connectionString=connectString;
		_dataUser=dataUser;
		_dataPassword= dataPassword;
		_address=address;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 776, 429);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("User Code");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(25, 32, 114, 24);
		contentPane.add(lblNewLabel);
		
		userTxt = new JTextField();
		userTxt.setBounds(161, 34, 125, 24);
		contentPane.add(userTxt);
		userTxt.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPassword.setBounds(25, 86, 114, 24);
		contentPane.add(lblPassword);
		
		pwdTxt = new JTextField();
		pwdTxt.setColumns(10);
		pwdTxt.setBounds(161, 88, 125, 24);
		contentPane.add(pwdTxt);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblType.setBounds(25, 148, 114, 24);
		contentPane.add(lblType);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"a", "m", "s", "e", "d"}));
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox.setBounds(161, 150, 125, 24);
		contentPane.add(comboBox);
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
	
		addButton.setBounds(25, 212, 89, 23);
		contentPane.add(addButton);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(361, 211, 374, 24);
		contentPane.add(textField_2);
		
		JLabel lblMessage = new JLabel("Message");
		lblMessage.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMessage.setBounds(264, 209, 75, 24);
		contentPane.add(lblMessage);
		
		JButton deleteButton = new JButton("Delete");
	
		deleteButton.setBounds(135, 212, 89, 23);
		contentPane.add(deleteButton);
		
		JButton okButton = new JButton("Ok");
	
		okButton.setBounds(25, 267, 89, 23);
		contentPane.add(okButton);
		
		JLabel lblAadminMmanagerDdipendente = new JLabel("a=admin; m=manager; d=dipendente; s=super; e=esterno");
		lblAadminMmanagerDdipendente.setBounds(355, 148, 364, 24);
		contentPane.add(lblAadminMmanagerDdipendente);
		
		JButton btnManageExternalUsers = new JButton("Manage external users");
	
		btnManageExternalUsers.setBounds(361, 35, 202, 23);
		contentPane.add(btnManageExternalUsers);
		
		/************************* eventi *************************/
		
		okButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closeWindow();
			}
		});
		btnManageExternalUsers.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) { //(String connectionString, String address, String dataUser, String dataPassword
				ExternalUser win = new ExternalUser(_connectionString, _address, _dataUser, _dataPassword);
				win.setVisible(true);
			}
		});
		
		deleteButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				boolean b=false;
				AdminInterface d = null;
				try {
					d = (AdminInterface)Naming.lookup(_address+"/AdminInt");
					
				//-------------------------------------------------------
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					
					String connectionString=SqlConnect.getConnectString(dataHost, dataName);
					//---------------------------------------------------- PROBLEMA
					b=d.deleteOnUserSql(userTxt.getText());
					//-------------------------------------------------------
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (b){
					textField_2.setText("User deleted");
				}
				else{
					textField_2.setText("Error");
				}
				
			}
		});
		
		
		
		
		addButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				boolean b = false;
				//---------------------------------------------------- PROBLEMA
				AdminInterface d = null;
				try {
					d = (AdminInterface)Naming.lookup(_address+"/AdminInt");
					
				//-------------------------------------------------------
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String connectionString= SqlConnect.getConnectString(dataHost, dataName);
					//---------------------------------------------------- PROBLEMA
					try {
					
						
						
						b=d.writeOnUsersSql( userTxt.getText(), pwdTxt.getText(), (String)comboBox.getSelectedItem());
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//-------------------------------------------------------
				
				if (b){
					textField_2.setText("User added");
				}
				else{
					textField_2.setText("Error");
				}
			}
		}
					
				
				
			
		);
		
	}


	
	protected void closeWindow() {
		// TODO Auto-generated method stub
		
		this.setVisible(false);
		
	}
}
