package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class ProjectModify extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProjectModify frame = new ProjectModify();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ProjectModify() {
		setTitle("Modify/Create Project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 673, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Project Number");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(30, 44, 137, 24);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setBounds(177, 46, 154, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(30, 99, 605, 2);
		contentPane.add(separator);
		
		JLabel lblCustomer = new JLabel("Customer");
		lblCustomer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCustomer.setBounds(30, 148, 137, 24);
		contentPane.add(lblCustomer);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox.setBounds(177, 148, 154, 24);
		contentPane.add(comboBox);
		
		JLabel lblDeadline = new JLabel("DeadLine");
		lblDeadline.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDeadline.setBounds(30, 194, 137, 24);
		contentPane.add(lblDeadline);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_1.setColumns(10);
		textField_1.setBounds(177, 194, 154, 24);
		contentPane.add(textField_1);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblType.setBounds(379, 148, 56, 24);
		contentPane.add(lblType);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox_1.setBounds(437, 148, 154, 24);
		contentPane.add(comboBox_1);
		
		JLabel lblManager = new JLabel("Manager");
		lblManager.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblManager.setBounds(379, 194, 85, 24);
		contentPane.add(lblManager);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox_2.setBounds(474, 194, 119, 24);
		contentPane.add(comboBox_2);
		
		JButton btnNewButton = new JButton("Save");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(30, 290, 89, 24);
		contentPane.add(btnNewButton);
		
		JButton btnCloseProject = new JButton("Close Project");
		btnCloseProject.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCloseProject.setBounds(30, 240, 212, 24);
		contentPane.add(btnCloseProject);
	}

}
