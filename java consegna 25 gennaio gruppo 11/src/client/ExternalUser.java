package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import classes.AdminInterface;
import classes.ProjectInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ExternalUser extends JFrame {

	private JPanel contentPane;
	private JTextField maxtxt;
	private JComboBox comboBox;

	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPassword;


	 ///c.getConnectString(_dataHost, _dataName), _address, _dataUser, _dataPassword);
	public ExternalUser(String connectionString, String address, String dataUser, String dataPassword) {
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPassword=dataPassword;
		setTitle("External User Privilege");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 469, 212);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMaxAccess = new JLabel("Max access");
		lblMaxAccess.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMaxAccess.setBounds(28, 23, 124, 24);
		contentPane.add(lblMaxAccess);
		
		maxtxt = new JTextField();
		maxtxt.setColumns(10);
		maxtxt.setBounds(148, 25, 200, 24);
		contentPane.add(maxtxt);
		
		JButton btnNewButton = new JButton("Set");

		btnNewButton.setBounds(27, 115, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit");

		btnExit.setBounds(160, 115, 89, 23);
		contentPane.add(btnExit);
		
		JLabel lblIdUser = new JLabel("Id User");
		lblIdUser.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdUser.setBounds(28, 58, 124, 24);
		contentPane.add(lblIdUser);
		
		final JComboBox comboBox = new JComboBox();
	
		comboBox.setBounds(148, 58, 200, 24);
		contentPane.add(comboBox);
		
		updatecombo();
		
		comboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				updatemaxtxt();
			}
		});
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closewin();
			}
		});
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AdminInterface e = null;
				
				
				try {
					e = (AdminInterface)Naming.lookup(_address+"/AdminInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					e.setExtPrivileges( (String)comboBox.getSelectedItem() ,Integer.valueOf(maxtxt.getText()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}

	protected void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}

	private void updatecombo() {
		// TODO Auto-generated method stub
		comboBox.removeAll();
	AdminInterface e = null;
		
		
		try {
			e = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<String> users=new ArrayList<String>();
		
		try {
			users=e.getExtUser();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<users.size(); i=i+1){
			comboBox.addItem(users.get(i));
		}
	}

	private void updatemaxtxt() {
		// TODO Auto-generated method stub
	AdminInterface e = null;
		
		
		try {
			e = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int maxacc = 0;
		try {
			maxacc = e.getMaxExtAccess( (String)comboBox.getSelectedItem());
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		maxtxt.setText(String.valueOf(maxacc));
		
	}
}
