package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import classes.Customer;
import classes.CustomerInterface;
import classes.Order;
import classes.ProjectInterface;
import classes.ProposteMaterialiInterface;
//import server.OrderInterface_;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OrderWin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField statustxt;
	private JTextField  lblSellPrice;
	private JComboBox projectCombo;
	private JComboBox customerCombo;
	
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPassword;


	 ///c.getConnectString(_dataHost, _dataName), _address, _dataUser, _dataPassword);
	public OrderWin(String connectionString, String address, String dataUser, String dataPassword) {
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPassword=dataPassword;
		setTitle("Order");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 707, 269);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Customer");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(26, 21, 131, 24);
		contentPane.add(lblNewLabel);
		
		customerCombo = new JComboBox();
	
		customerCombo.setBounds(132, 23, 200, 24);
		contentPane.add(customerCombo);
		
		JLabel lblProject = new JLabel("Project");
		lblProject.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProject.setBounds(26, 72, 131, 24);
		contentPane.add(lblProject);
		
		projectCombo = new JComboBox();
	
		projectCombo.setBounds(132, 74, 200, 24);
		contentPane.add(projectCombo);
		
		final JLabel lblSellPrice = new JLabel("Sell Price");
		lblSellPrice.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSellPrice.setBounds(357, 21, 131, 24);
		contentPane.add(lblSellPrice);
		
		textField = new JTextField();
		textField.setBounds(439, 21, 200, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		statustxt = new JTextField();
		statustxt.setColumns(10);
		statustxt.setBounds(439, 72, 200, 24);
		contentPane.add(statustxt);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStatus.setBounds(357, 72, 131, 24);
		contentPane.add(lblStatus);
		
		JButton btnNewButton = new JButton("Add");
	
		btnNewButton.setBounds(44, 162, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnModify = new JButton("Modify");
		
		btnModify.setBounds(160, 162, 89, 23);
		contentPane.add(btnModify);
		
		JButton btnExit = new JButton("Exit");
	
		btnExit.setBounds(281, 162, 89, 23);
		contentPane.add(btnExit);
		
		updatecombobox();
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				closewin();
			}
		});
		
		customerCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				findorder();
			}
		});
		
		projectCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				findorder();
			}
		});
		
		btnModify.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProjectInterface e=null;
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					e.modifyOrder( (Integer)projectCombo.getSelectedItem(), (String)customerCombo.getSelectedItem(),Double.valueOf(textField.getText()) , statustxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProjectInterface e=null;
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					//System.out.println("Double.valueOf(lblSellPrice.getText() "+Integer.valueOf(lblSellPrice.getText()));
					e.addOrder((Integer)projectCombo.getSelectedItem(), (String)customerCombo.getSelectedItem(),Double.valueOf(textField.getText())  , statustxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
	}
	
	protected void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}

	protected void findorder() {
		// TODO Auto-generated method stub
		ProjectInterface e=null;
		try {
			e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Order order=null;
		try {
			order= e.getOrder( (Integer)projectCombo.getSelectedItem() , (String)customerCombo.getSelectedItem());
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (order!= null){
			textField.setText(String.valueOf(order.get_sellPrice()));
			statustxt.setText(order.get_status());
		}
		
	}

	private void updatecombobox(){
		
		customerCombo.removeAllItems();
		projectCombo.removeAllItems();
		ArrayList<String> customers = new ArrayList<String>();
		ArrayList<Integer> projects = new ArrayList<Integer>();
		CustomerInterface e = null;
		ProjectInterface f = null;
		
		
		try {
			e = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String cust="";
		
		try {
			customers= e.getCustomersStringList();
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		//customers=FileManagerClient.fromStringToStringArray(cust);
	
		
		for (int i=0; i<customers.size(); i=i+1){
			
			customerCombo.addItem(customers.get(i));
		}
		
		try {
			f=(ProjectInterface)Naming.lookup(_address+"/ProjectInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			projects=f.getIdProjectsList();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
			for (int i=0; i<projects.size(); i=i+1){
			
			projectCombo.addItem(projects.get(i));
		}
	}
	
}
