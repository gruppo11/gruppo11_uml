package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ConnectWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private boolean ok;
	private String address;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConnectWindow frame = new ConnectWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConnectWindow() {
		setOk(false);
		setAddress("localhost");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 782, 119);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Remote Server");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(26, 11, 114, 24);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setBounds(153, 13, 288, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Connect");

		btnNewButton.setBounds(566, 13, 116, 24);
		contentPane.add(btnNewButton);
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			
			//gestione click su connetti
			@Override
			public void mouseClicked(MouseEvent arg0) {
			String string= null;//"localhost";	
			if (textField.getText().contentEquals("")){
				string="localhost";
			}
			else
			{
				string=textField.getText();
			}
			
			
			setAddress(("rmi://"+string+":1099"));   //   /LoginInt"));	
			setOk(true);	
				
			}
		});
				
	}

	public boolean isOk() {
		return ok;
	}


	
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
